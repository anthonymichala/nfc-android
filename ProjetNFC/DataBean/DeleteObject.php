<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeleteObject
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../Modele/Panier/LigneCommande.php');
require_once('../Service/ProduitService.php');
class DeleteObject {
    public static function delete($connection, $nameTable, $id){
        if($this instanceof LigneCommande){
            $ligneCommande = new LigneCommande();
            $result = $ligneCommande->rechercher($connection, " AND id=".$id);
            $ligneCommandePrincipal = pg_fetch_row($result,NULL, PGSQL_ASSOC);
            $quantite = $ligneCommandePrincipal['quantite']*(-1);
            $produit = $ligneCommandePrincipal['ProduitId'];
            ProduitService::majStock($connection, $produit, $quantite);
        }
        $query = "DELETE FROM \"".$nameTable."\" WHERE id='".$id."'";
        pg_query($connection, $query);
    }
}
