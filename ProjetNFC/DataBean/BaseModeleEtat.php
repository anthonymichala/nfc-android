<?php
require_once('../DataBean/BaseModele.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseModeleEtat
 *
 * @author anthonyandrianaivoravelona
 */
class BaseModeleEtat extends BaseModele{
    private $etat;
    function __construct() {
        
    }
    function getEtat() {
        return $this->etat;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }
    public function updateEtat($connection, $etat){
        $dbGen  = new DBGen();
        return $dbGen->updateEtat($connection, $this->getNomTable(), $this->getId(), $etat);
    }
    
}
