<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DBGen
 *
 * @author anthonyandrianaivoravelona
 */
require_once ('../Utilitaire/UtilitaireConnexion.php');
require_once ('../Modele/Panier/LigneCommande.php');
require_once ('../DataBean/BaseModele.php');
class DBGen {
    public function rechercher(BaseModele $baseModele, $connection, $apresWhere){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $query = "SELECT * FROM \"".$baseModele->getNomTable()."\" where 1<2 ".$apresWhere."";
            return pg_query($connection, $query);
        }
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updateEtat($connection, $nameTable, $id, $etat){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $query = "UPDATE \"".$nameTable."\" set etat=".$etat." WHERE id='".$id."'";
            return pg_query($connection, $query);
        }
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function delete($connection, $baseModele){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            if($baseModele->getNomTable()=="LigneCommande"){
                $ligneCommande = new LigneCommande();
                $result = $ligneCommande->rechercher($connection, " AND id=".$baseModele->getId());

                $ligneCommandePrincipal = pg_fetch_row($result,NULL, PGSQL_ASSOC);
                $quantite = $ligneCommandePrincipal['quantite']*(-1);
                $produit = $ligneCommandePrincipal['ProduitId'];
                ProduitService::majStock($connection, $produit, $quantite);
            }
            $query = "DELETE FROM \"".$baseModele->getNomTable()."\" WHERE id='".$baseModele->getId()."'";
            pg_query($connection, $query);
        }
        catch (Exception $ex) {
            throw $ex;
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public static function updatePhoto($connection, BaseModele $baseModele, $fichierSurServeur, $chemin, $fichierNew){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $chemin .= $fichierNew;
            $resultat = move_uploaded_file($fichierSurServeur,$chemin);
            if(!$resultat){
                throw new Exception("Erreur durant le transfert du fichier");
            }
            $query = "UPDATE \"".$baseModele->getNomTable()."\" set image='".$fichierNew."'where id=".$baseModele->getId();
            pg_query($connection, $query);
            if($_SESSION['idLogin']==$baseModele->getId()){
                $_SESSION['image'] = $fichierNew;
            }
        }
        catch (Exceptions $ex) {
            throw $ex;
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
        
    }
}
