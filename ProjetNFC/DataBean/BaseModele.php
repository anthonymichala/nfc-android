<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseModele
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../DataBean/DBGen.php');
class BaseModele {
    private $id;
    private $nomTable;
    function __construct() {
    }
    function getNomTable() {
        return $this->nomTable;
    }
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNomTable($nomTable) {
        $this->nomTable = $nomTable;
    }
    public function rechercher($connection, $apresWhere){
        $dbGen  = new DBGen();
        return $dbGen->rechercher($this, $connection , $apresWhere);
    }
    public function delete($connection){
        $dbGen  = new DBGen();
        $dbGen->delete($connection, $this);
    }
    public function getValSequence($connection){
        try{
            $nameSequence = "seq_".$this->getNomTable();
            $query = "select nextval('\"".$nameSequence."\"')";
            $dataReader = pg_query($connection, $query);
            $row = pg_fetch_row($dataReader);
            return $row[0];
        } catch (Exception $ex) {
            throw $ex->getMessage();
        }
    }
}
