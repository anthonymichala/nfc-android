
<div class="sidebar" data-background-color="brown" data-active-color="danger">
    <div class="logo">
        <a href="smarket.php?page=accueil.php" class="simple-text">Smarket</a>
    </div>
    <div class="logo logo-mini">
        <a href="smarket.php?page=accueil.php" class="simple-text">SK</a>
    </div>
    <div class="sidebar-wrapper">
            <div class="user">
                <div class="photo">
                    <img src="../assets/img/users/<?php echo $_SESSION['image']?>" />
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="charts.html#collapseExample" class="collapsed">
                        <?php echo $_SESSION['login'];?>
                        <b class="caret"></b>
                    </a>
                    <div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li><a href="smarket.php?page=users/profil-update.php">Modifier Profil</a></li>
                            <li><a href="deconnexion.php">Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav">
                <li>
                    <a href="smarket.php?page=rayon/rayon-liste.php">
                        <i class="ti-package"></i>
                        <p>Rayon</p>
                    </a>
                </li>
                <li>
                    <a href="smarket.php?page=produit/produit-liste.php">
                        <i class="ti-shopping-cart"></i>
                        <p>Produit</p>
                    </a>
                </li>
                <li>
                    <a href="smarket.php?page=panier/panier-liste.php">
                        <i class="ti-shopping-cart-full"></i>
                        <p>Panier</p>
                    </a>
                </li>
                <li>
                    <a href="smarket.php?page=caisse/caisse-liste.php">
                        <i class="ti-credit-card"></i>
                        <p>Caisse</p>
                    </a>
                </li>
                <?php if($_SESSION['typeIdentification']==3){ ?>
                <li>
                    <a data-toggle="collapse" href="charts.html#dashboardOverview">
                        <i class="ti-user"></i>
                        <p>Utilisateur <b class="caret"></b></p>
                    </a>
                    <div class="collapse" id="dashboardOverview">
                        <ul class="nav">
                            <li><a href="smarket.php?page=users/profil-liste.php">Profil</a></li>
                            <li><a href="smarket.php?page=users/typeProfil-liste.php">Type Profil</a></li>
                        </ul>
                    </div>
                </li>
                <?php } ?>
            </ul>
    </div>
</div>