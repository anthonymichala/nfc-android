<nav class="navbar navbar-default">
    <div class="container-fluid">
                        <div class="navbar-minimize">
                                <button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
                        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand" href="#">ADMINISTRATION SMARKET</a>
        </div>
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle btn-rotate" data-toggle="dropdown">
                        <i class="ti-bell"></i>
                        <span class="notification">0</span>
                        <p class="hidden-md hidden-lg">
                                Notifications
                                <b class="caret"></b>
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>