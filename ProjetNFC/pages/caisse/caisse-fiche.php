<?php 
require_once('../Modele/Caisse/Caisse.php');
$id = $_GET['id'];
$caisse = new Caisse();
$table = $caisse->getNomTable();
$caisse->setNomTable("caisse_libelle");
$results = $caisse->rechercher(NULL, " AND ID='".$id."'");
$caisseObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC);
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=caisse/caisse-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Fiche Caisse Numéro <?php echo $id?></h3>
        </div>
        <div class="content">
            <div class="row">
                <table class="table table-bordered">
                    <tr>
                        <th>Numéro de la caisse</th>
                        <td><?php echo $caisseObjet['numeroCaisse']?></td>
                    </tr>
                    <tr>
                        <th>Remarque</th>
                        <td><?php echo $caisseObjet['remarque']?></td>
                    </tr>
                    <tr>
                        <th>Caissier</th>
                        <td><?php echo $caisseObjet['caissierId']?></td>
                    </tr>
                    <tr>
                        <th>Etat</th>
                        <td><?php echo $caisse->getEtatTexte($caisseObjet['etat'])?></td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <a href="deleteGen.php?id=<?php echo $id?>&but=caisse/caisse-liste.php&nomTable=<?php echo $table?>" class="btn btn-danger pull-right">Supprimer</a>
                <a href="smarket.php?id=<?php echo $id?>&page=caisse/caisse-update.php" class="btn btn-warning pull-right">Modifier</a>
                <?php
                if($caisseObjet['etat']==0){
                ?>
                    <a href="updateEtatGen.php?id=<?php echo $id?>&but=caisse/caisse-fiche.php&nomTable=<?php echo $table?>&etat=1" class="btn btn-primary pull-right">Mettre en service</a>
                <?php
                }
                else{
                ?>
                    <a href="updateEtatGen.php?id=<?php echo $id?>&but=caisse/caisse-fiche.php&nomTable=<?php echo $table?>&etat=0" class="btn btn-danger pull-right">Mettre hors service</a>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>