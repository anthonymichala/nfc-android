<?php 
require_once('../Modele/Users/Identification.php');
$identification = new Identification();
$resultsIdentification = $identification->rechercher(NULL, " AND \"typeIdentification\"=2");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=caisse/caisse-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Ajouter une Caisse</h3>
        </div>
        <div class="content">
            <form method="get" action="smarket.php">
                <input type="hidden" name="page" value="caisse/apresCaisse.php"/>
                <input type="hidden" name="action" value="insert"/>
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Numéro de la Caisse</th>
                            <td><input type="text" name="numeroCaisse" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Remarque</th>
                            <td><input type="text" name="remarque" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Caissier</th>
                            <td>
                                <select name="CaissierId" class="selectpicker" data-title="Caissier" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Caissier = pg_fetch_row($resultsIdentification,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Caissier['id']?>" ><?php echo $Caissier['nom']." ".$Caissier['prenom']?></option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-success pull-right" value="Ajouter" />
                </div>
            </form>
        </div>
    </div>
</div>