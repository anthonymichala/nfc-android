<?php 
require_once('../Modele/Caisse/Caisse.php');
require_once('../Modele/Users/Identification.php');
$id = $_GET['id'];
$caisse = new Caisse();
$table = $caisse->getNomTable();
$results = $caisse->rechercher(NULL, " AND ID='".$id."'");
$caisseObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC);
$identification = new Identification();
$resultsIdentification = $identification->rechercher(NULL, " AND \"typeIdentification\"=2");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=caisse/caisse-fiche.php&id=<?php echo $id?>" class="btn btn-primary"><i class="ti-back-left"></i></a> Modifier Caisse Numéro <?php echo $id?></h3>
        </div>
        <div class="content">
            <form method="get" action="smarket.php">
                <input type="hidden" name="page" value="caisse/apresCaisse.php"/>
                <input type="hidden" name="action" value="update"/>
                <input type="hidden" name="id" value="<?php echo $id?>"/>
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Numéro de la caisse</th>
                            <td><input type="text" name="numeroCaisse" id="valeur" class="form-control" value="<?php echo $caisseObjet['numeroCaisse']?>" /></td>
                        </tr>
                        <tr>
                            <th>Remarque</th>
                            <td><input type="text" name="remarque" id="valeur" class="form-control" value="<?php echo $caisseObjet['remarque']?>" /></td>
                        </tr>
                        <tr>
                            <th>Chef de Rayon</th>
                            <td>
                                <select name="CaissierId" class="selectpicker" data-title="Caissier" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Caissier = pg_fetch_row($resultsIdentification,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Caissier['id']?>" <?php if($caisseObjet['caissierId']==$Caissier['id']){ echo "selected='selected'";}?>><?php echo $Caissier['nom']." ".$Caissier['prenom']?></option>  
                                    <?php
                                    }
                                    ?>

                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-warning pull-right" value="Confirmer les modifications">
                </div>
            </form>
        </div>
    </div>
</div>