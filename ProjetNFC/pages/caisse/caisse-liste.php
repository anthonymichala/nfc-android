<?php
require('../Modele/Caisse/Caisse.php');
$caisse = new Caisse();
$table = $caisse->getNomTable();
$caisse->setNomTable("caisse_libelle");
$results = $caisse->rechercher(NULL, "");
?>
<h1>Liste des caisses</h1>
<div class="card">
    <div class="content">
        <a href="smarket.php?page=caisse/caisse-saisie.php" class="btn btn-primary pull-right"><i class="ti-plus"></i> Ajouter</a>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
                <tr>
                    <th>Identification</th>
                    <th>Numéro de Caisse</th>
                    <th>Remarque</th>
                    <th>Caissier</th>
                    <th>Etat</th>
                    <th class="disabled-sorting">Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Identification</th>
                    <th>Numéro de Caisse</th>
                    <th>Remarque</th>
                    <th>Caissier</th>
                    <th>Etat</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                <?php while($caisseObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC)){ ?>
                <tr>
                    <td><a href="smarket.php?page=caisse/caisse-fiche.php&id=<?php echo $caisseObjet['id']?>"><?php echo $caisseObjet['id']?></a></td>
                    <td><?php echo $caisseObjet['numeroCaisse']?></td>
                    <td><?php echo $caisseObjet['remarque']?></td>
                    <td><?php echo $caisseObjet['caissierId']?></td>
                    <td><?php echo $caisse->getEtatTexte($caisseObjet['etat'])?></td>
                    <td>
                        <a href="smarket.php?id=<?php echo $caisseObjet['id']?>&page=caisse/caisse-update.php" class="btn btn-simple btn-warning btn-icon"><i class="ti-pencil-alt"></i></a>
                        <a href="deleteGen.php?id=<?php echo $caisseObjet['id']?>&but=<?php echo $_GET['page']?>&nomTable=<?php echo $table?>" class="btn btn-simple btn-danger btn-icon"><i class="ti-close"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</div>