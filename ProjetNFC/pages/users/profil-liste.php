<?php
require('../Modele/Users/Identification.php');
$identification = new Identification();
$table = $identification->getNomTable();
$identification->setNomTable("identification_libelle");
$results = $identification->rechercher(NULL, "");
?>
<h1>Liste des utilisateurs</h1>
<div class="card">
    <div class="content">
        <a href="smarket.php?page=users/profil-saisie.php" class="btn btn-primary pull-right"><i class="ti-plus"></i> Ajouter</a>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
                <tr>
                    <th>Identification</th>
                    <th>Image</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Login</th>
                    <th>Type d'Identification</th>
                    <th>Etat</th>
                    <th class="disabled-sorting">Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Identification</th>
                    <th>Image</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Login</th>
                    <th>Type d'Identification</th>
                    <th>Etat</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                <?php while($profil = pg_fetch_row($results,NULL, PGSQL_ASSOC)){ ?>
                <tr>
                    <td><a href="smarket.php?page=users/profil-update.php&id=<?php echo $profil['id']?>"><?php echo $profil['id']?></a></td>
                    <td><img src="../assets/img/users/<?php echo $profil['image']?>"></td>
                    <td><?php echo $profil['nom']?></td>
                    <td><?php echo $profil['prenom']?></td>
                    <td><?php echo $profil['login']?></td>
                    <td><?php echo $profil['typeidentification']?></td>
                    <td><?php echo $identification->getEtatTexte($profil['etat'])?></td>
                    <td>
                        <a href="smarket.php?page=users/profil-update.php&id=<?php echo $profil['id']?>" class="btn btn-simple btn-warning btn-icon"><i class="ti-pencil-alt"></i></a>
                        <a href="deleteGen.php?id=<?php echo $profil['id']?>&but=<?php echo $_GET['page']?>&nomTable=<?php echo $table?>" class="btn btn-simple btn-danger btn-icon"><i class="ti-close"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</div>