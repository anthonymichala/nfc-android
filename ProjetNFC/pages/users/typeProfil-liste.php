<?php
require('../Modele/Users/TypeProfil.php');
$typeProfil = new TypeProfil();
$table = $typeProfil->getNomTable();
$results = $typeProfil->rechercher(NULL, "");
?>
<h1>Liste des Types de profil</h1>
<div class="card">
    <div class="content">
        <a href="smarket.php?page=users/typeProfil-saisie.php" class="btn btn-primary pull-right"><i class="ti-plus"></i> Ajouter</a>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Valeur</th>
                    <th>Description</th>
                    <th class="disabled-sorting">Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Valeur</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                <?php while($profil = pg_fetch_row($results,NULL, PGSQL_ASSOC)){ ?>
                <tr>

                    <td><?php echo $profil['id']?></td>
                    <td><?php echo $profil['val']?></td>
                    <td><?php echo $profil['desce']?></td>
                    <td>
                        <a href="smarket.php?id=<?php echo $profil['id']?>&page=users/typeProfil-update.php" class="btn btn-simple btn-warning btn-icon"><i class="ti-pencil-alt"></i></a>
                        <a href="deleteGen.php?id=<?php echo $profil['id']?>&but=<?php echo $_GET['page']?>&nomTable=<?php echo $table?>" class="btn btn-simple btn-danger btn-icon"><i class="ti-close"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</div>