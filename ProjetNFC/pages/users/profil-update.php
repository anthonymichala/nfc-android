<?php
    require('../Modele/Users/Identification.php');
    require('../Modele/Users/TypeProfil.php');
    //require('../Utilitaire/UtilitaireConnexion.php');
    $utilitaireConnexion = new UtilitaireConnexion();
    $connection = $utilitaireConnexion->getConn();
    $id = !isset($_GET['id'])?$_SESSION['idLogin']:$_GET['id'];
    $identification = new Identification();
    $results = $identification->rechercher($connection, " AND id='".$id."'");
    $personnel = pg_fetch_row($results,NULL, PGSQL_ASSOC);
    $profil = new TypeProfil();
    $resultsProfil = $profil->rechercher($connection, "");
?>
<div class="row">
    <form method="post" action="smarket.php?page=users/apresUtilisateur.php" enctype="multipart/form-data">
    <div class="col-lg-4 col-md-5">
        <div class="card card-user">
            <div class="image">
                <img src="../assets/img/background.jpg" alt="..."/>
            </div>
            <div class="content">
                <div class="author">
                    <img class="avatar border-white" src="../assets/img/users/<?php echo $personnel['image']?>" alt="..."/>
                    <input type="file" name="pdp" id="file-1" class="inputfile btn btn-primary" data-multiple-caption="{count} files selected" multiple />
                    <label class="btn btn-primary" for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Changer la photo de profil</span></label>
                    <h4 class="title">
                            <?php echo $personnel['nom']." ".$personnel['prenom'];?><br />
                        <a href="user.html#"><small>@<?php echo $personnel['login']?></small></a>
                    </h4>
                </div>
            </div>
            <hr>
        </div>
        </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <div class="header">
                <h4 class="title">Modifier le Profil</h4>
            </div>
            <div class="content">
                    <input type="hidden" name="object" value="identification" />
                    <input type="hidden" name="action" value="update"/>
                    <input type="hidden" name="id" value="<?php echo $id?>"/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pseudonyme</label>
                                <input type="text" name="login"class="form-control border-input" placeholder="Username" value="<?php echo $personnel['login']?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mot de passe</label>
                                <input type="password" name="mdp" class="form-control border-input" placeholder="Mot de passe" value="<?php echo $personnel['mdp']?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Confirmer le mot de passe</label>
                                <input type="password" class="form-control border-input" placeholder="Confirmation Mot de passe" value="<?php echo $personnel['mdp']?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nom</label>
                                <input type="text" name="nom" class="form-control border-input" placeholder="Nom" value="<?php echo $personnel['nom']?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Prénom</label>
                                <input type="text" name="prenom" class="form-control border-input" placeholder="Prénom" value="<?php echo $personnel['prenom']?>">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Profil</label>
                                <select <?php if($_SESSION['typeIdentification']!=3){ echo "disabled"; }?> name="typeIdentification" class="selectpicker" data-title="Profil Select" data-style="btn-danger btn-block" data-menu-style="dropdown-blue">
                                    <?php while($profils = pg_fetch_row($resultsProfil,NULL, PGSQL_ASSOC)){ ?>
                                        <option value="<?php echo $profils['id']?>" <?php if($profils['id']==$personnel['typeIdentification']){ echo "selected='selected'";}?>><?php echo $profils['val']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-info btn-fill btn-wd">Modifier</button>
                        <a href="deleteGen.php?id=<?php echo $personnel['id']?>&but=users/profil-liste.php&nomTable=<?php echo $identification->getNomTable()?>" class="btn btn-fill btn-wd btn-danger">Supprimer</a>
                    </div>
                    <div class="clearfix"></div>
                
            </div>
        </div>
    </div>
    </form>
</div>