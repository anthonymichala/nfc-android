<?php

require_once('../Service/IdentificationService.php');
$action = $_POST['action'];
$object = $_POST['object'];
$id = "";
$identificationService = new IdentificationService();
$fiche = "smarket.php?";
if($object=="typeProfil"){
    $valeur = $_POST['valeur'];
    $description = $_POST['description'];
    if($action=="insert"){
        $id = $identificationService->insertTypeProfil(NULL, $valeur, $description);
    }
    else if($action=="update"){
        $id = $_POST['id'];
        $identificationService->updateTypeProfil(NULL, $id, $valeur, $description);
    }
    $fiche .= "page=users/typeProfil-liste.php&id=".$id;
}
else if($object=="identification"){
    $login = $_POST['login'];
    $mdp = $_POST['mdp']; 
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $typeIdentification = $_POST['typeIdentification']; 
    if($action=="insert"){
        $id = $identificationService->insertUser(NULL, $login, $mdp, $nom, $prenom, $typeIdentification);
    }
    else if($action=="update"){
        $id = $_POST['id'];
        $identificationService->updateUser(NULL, $id, $login, $mdp, $nom, $prenom, $typeIdentification);
    }
    if(isset($_FILES['pdp']['tmp_name']) && $_FILES['pdp']['tmp_name']!=""){
        $nomFichierServeur = $_FILES['pdp']['tmp_name'];
        $type = $_FILES['pdp']['type'];
        $identificationService->updatePdpClient($nomFichierServeur, $type, $id);
    }
    $fiche .= "page=users/profil-update.php&id=".$id;
}
?>
<script>
    document.location.replace("<?php echo $fiche ?>");
</script>
