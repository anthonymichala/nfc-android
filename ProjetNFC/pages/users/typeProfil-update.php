<?php 
require_once('../Modele/Users/TypeProfil.php');
$id = $_GET['id'];
$typeProfil = new TypeProfil();
$results = $typeProfil->rechercher(NULL, " AND id='".$id."'");
$typeProfilObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC);
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=users/typeProfil-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Modifier Type de Profil Numéro <?php echo $id?></h3>
        </div>
        <div class="content">
            <form method="post" action="smarket.php?page=users/apresUtilisateur.php">
                <input type="hidden" name="object" value="typeProfil"/>
                <input type="hidden" name="action" value="update"/>
                <input type="hidden" name="id" value="<?php echo $id?>"/>
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Valeur</th>
                            <td><input type="text" name="valeur" id="valeur" class="form-control" value="<?php echo $typeProfilObjet['val']?>" /></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><input type="text" name="description" id="valeur" class="form-control" value="<?php echo $typeProfilObjet['desce']?>" /></td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-warning pull-right" value="Confirmer les modifications">
                </div>
            </form>
        </div>
    </div>
</div>