<?php
    require('../Modele/Users/TypeProfil.php');
    $profil = new TypeProfil();
    $resultsProfil = $profil->rechercher(NULL, "");
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Ajouter Profil</h4>
            </div>
            <div class="content">
                <form method="post" action="smarket.php?page=users/apresUtilisateur.php">
                    <input type="hidden" name="object" value="identification" />
                    <input type="hidden" name="action" value="insert"/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pseudonyme</label>
                                <input type="text" name="login"class="form-control border-input" placeholder="Username">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mot de passe</label>
                                <input type="password" name="mdp" class="form-control border-input" placeholder="Mot de passe">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Confirmer le mot de passe</label>
                                <input type="password" class="form-control border-input" placeholder="Confirmation Mot de passe">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nom</label>
                                <input type="text" name="nom" class="form-control border-input" placeholder="Nom">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Prénom</label>
                                <input type="text" name="prenom" class="form-control border-input" placeholder="Prénom">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Profil</label>
                                <select name="typeIdentification" class="selectpicker" data-title="Profil Select" data-style="btn-danger btn-block" data-menu-style="dropdown-blue">
                                    <?php while($profils = pg_fetch_row($resultsProfil,NULL, PGSQL_ASSOC)){ ?>
                                        <option value="<?php echo $profils['id']?>" ><?php echo $profils['val']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-info btn-fill btn-wd">Ajouter</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>