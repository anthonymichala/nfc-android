<?php 
require_once('../Modele/Users/Identification.php');
require_once('../Modele/Caisse/Caisse.php');
$identification = new Identification();
$resultsIdentification = $identification->rechercher(NULL, " AND \"typeIdentification\"=1");
$caisse = new Caisse();
$resultsCaisse = $caisse->rechercher(NULL, "");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=panier/panier-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Ajouter un Panier</h3>
        </div>
        <div class="content">
            <form method="get" action="smarket.php">
                <input type="hidden" name="page" value="panier/apresPanier.php"/>
                <input type="hidden" name="object" value="panier"/>
                <input type="hidden" name="action" value="insert"/>
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Date du Panier</th>
                            <td><input type="text" name="datePanier" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Client</th>
                            <td>
                                <select name="clientId" class="selectpicker" data-title="Client" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Client = pg_fetch_row($resultsIdentification,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Client['id']?>" ><?php echo $Client['nom']." ".$Client['prenom']?></option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Caissier</th>
                            <td>
                                <select name="caisseId" class="selectpicker" data-title="Caisse" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Caisse = pg_fetch_row($resultsCaisse,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Caisse['id']?>" ><?php echo $Caisse['numeroCaisse']?></option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-success pull-right" value="Ajouter" />
                </div>
            </form>
        </div>
    </div>
</div>