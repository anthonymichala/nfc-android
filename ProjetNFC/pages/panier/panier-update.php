<?php 
require_once('../Modele/Users/Identification.php');
require_once('../Modele/Caisse/Caisse.php');
require_once('../Modele/Panier/Panier.php');
$id = $_GET['id'];
$panier = new Panier();
$resultPanier = $panier->rechercher(NULL, " AND id=".$id);
$panierObjet = pg_fetch_row($resultPanier,NULL, PGSQL_ASSOC);
$identification = new Identification();
$resultsIdentification = $identification->rechercher(NULL, " AND \"typeIdentification\"=1");
$caisse = new Caisse();
$resultsCaisse = $caisse->rechercher(NULL, "");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=panier/panier-fiche.php&id=<?php echo $id?>" class="btn btn-primary"><i class="ti-back-left"></i></a> Modifier Panier Numéro <?php echo $id?></h3>
        </div>
        <div class="content">
            <form method="get" action="smarket.php">
                <input type="hidden" name="page" value="panier/apresPanier.php"/>
                <input type="hidden" name="action" value="update"/>
                <input type="hidden" name="id" value="<?php echo $id?>"/>
                <input type="hidden" name="object" value="panier"/>
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Date du panier</th>
                            <td><input type="text" name="datePanier" id="valeur" class="form-control" value="<?php echo $panierObjet['datePanier']?>" /></td>
                        </tr>
                        <tr>
                            <th>Client</th>
                            <td>
                                <select name="clientId" class="selectpicker" data-title="Client" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Client = pg_fetch_row($resultsIdentification,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Client['id']?>" <?php if($panierObjet['clientId']==$Client['id']){ echo "selected='selected'";}?>><?php echo $Client['nom']." ".$Client['prenom']?></option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Caisse</th>
                            <td>
                                <select name="caisseId" class="selectpicker" data-title="Caisse" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Caisse = pg_fetch_row($resultsCaisse,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Caisse['id']?>" <?php if($panierObjet['caisseId']==$Caisse['id']){ echo "selected='selected'";}?>><?php echo $Caisse['numeroCaisse']?></option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-warning pull-right" value="Confirmer les modifications">
                </div>
            </form>
        </div>
    </div>
</div>