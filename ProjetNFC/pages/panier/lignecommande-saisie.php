<?php 
require_once('../Modele/Produit/Produit.php');
$id = $_GET['id'];
$produit = new Produit();
$resultsProduit = $produit->rechercher(NULL, "");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=panier/panier-fiche.php&id=<?php echo $id?>" class="btn btn-primary"><i class="ti-back-left"></i></a> Ajouter une Ligne de Commande sur le Panier numéro <?php echo $id?></h3>
        </div>
        <div class="content">
            <form method="get" action="smarket.php">
                <input type="hidden" name="page" value="panier/apresPanier.php"/>
                <input type="hidden" name="object" value="lignecommande"/>
                <input type="hidden" name="action" value="insert"/>
                <input type="hidden" name="PanierId" value="<?php echo $id?>"/>
                <input type="hidden" name="object" value="lignecommande" />
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Produit</th>
                            <td>
                                <select name="ProduitId" class="selectpicker" data-title="Produit" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Produit = pg_fetch_row($resultsProduit,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Produit['id']?>" ><?php echo $Produit['libelle']?> (<?php echo $Produit['quantiteStock']?> dispo(s)</option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Quantité</th>
                            <td><input type="text" name="quantite" id="valeur" class="form-control" /></td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-success pull-right" value="Ajouter" />
                </div>
            </form>
        </div>
    </div>
</div>