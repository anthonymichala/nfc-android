<?php

require_once('../Service/PanierService.php');
$action = $_GET['action'];
$object = $_GET['object'];
$id = "";
$panierService = new PanierService();

if($object=="panier"){
    $datePanier = $_GET['datePanier'];
    $clientId = $_GET['clientId'];
    $caisseId = $_GET['caisseId'];
    if($action=="insert"){
        $id = $panierService->insertPanier(NULL, $datePanier, $clientId, $caisseId);
    }
    else if($action=="update"){
        $id = $_GET['id'];
        $panierService->updatePanier(NULL, $id, $datePanier, $clientId, $caisseId);
    }
}
else if($object=="lignecommande"){
    $produit = $_GET['ProduitId'];
    $quantite = $_GET['quantite']; 
    $PanierId = $_GET['PanierId'];
    if($action=="insert"){
        $id = $panierService->insertLigneCommande(NULL, $produit, $quantite, $PanierId);
    }
    else if($action=="update"){
        $id = $_GET['id'];
        $panierService->updateLigneCommande(NULL, $id, $produit, $quantite, $PanierId);
        $id = $PanierId;
    }
}
$fiche = "smarket.php?page=panier/panier-fiche.php&id=".$id;
echo $fiche;
?>
<script>
    document.location.replace("<?php echo $fiche ?>");
</script>
