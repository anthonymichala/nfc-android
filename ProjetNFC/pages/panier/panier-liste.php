<?php
require('../Modele/Panier/Panier.php');
$panier = new Panier();
$table = $panier->getNomTable();
$panier->setNomTable("panier_libelle");
$results = $panier->rechercher(NULL, "");
?>
<h1>Liste des paniers</h1>
<div class="card">
    <div class="content">
        <a href="smarket.php?page=panier/panier-saisie.php" class="btn btn-primary pull-right"><i class="ti-plus"></i> Ajouter</a>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
                <tr>
                    <th>Identification</th>
                    <th>Date du panier</th>
                    <th>Client</th>
                    <th>Caisse</th>
                    <th>Etat</th>
                    <th class="disabled-sorting">Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Identification</th>
                    <th>Date du panier</th>
                    <th>Client</th>
                    <th>Caisse</th>
                    <th>Etat</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                <?php while($panierObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC)){ ?>
                <tr>
                    <td><a href="smarket.php?page=panier/panier-fiche.php&id=<?php echo $panierObjet['id']?>"><?php echo $panierObjet['id']?></a></td>
                    <td><?php echo $panierObjet['datePanier']?></td>
                    <td><?php echo $panierObjet['clientId']?></td>
                    <td><?php echo $panierObjet['caisseId']?></td>
                    <td><?php echo $panier->getEtatTexte($panierObjet['etat'])?></td>
                    <td>
                        <a href="smarket.php?page=panier/panier-update.php&id=<?php echo $panierObjet['id']?>" class="btn btn-simple btn-warning btn-icon "><i class="ti-pencil-alt"></i></a>
                        <a href="deleteGen.php?id=<?php echo $panierObjet['id']?>&but=<?php echo $_GET['page']?>&nomTable=<?php echo $table?>" class="btn btn-simple btn-danger btn-icon"><i class="ti-close"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</div>