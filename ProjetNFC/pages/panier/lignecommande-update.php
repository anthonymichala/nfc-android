<?php 
require_once('../Modele/Produit/Produit.php');
require_once('../Modele/Panier/LigneCommande.php');
$id = $_GET['id'];
$lignecommande = new LigneCommande();
$results = $lignecommande->rechercher(NULL, " AND ID='".$id."'");
$lignecommandeObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC);
$panierId = $lignecommandeObjet['PanierId'];
$produit = new Produit();
$resultProduit = $produit->rechercher(NULL, "");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=panier/panier-fiche.php&id=<?php echo $panierId?>" class="btn btn-primary"><i class="ti-back-left"></i></a> Modifier Ligne Commande Numéro <?php echo $id?> du Panier Numéro <?php echo $panierId?></h3>
        </div>
        <div class="content">
            <form method="get" action="smarket.php">
                <input type="hidden" name="page" value="panier/apresPanier.php"/>
                <input type="hidden" name="action" value="update"/>
                <input type="hidden" name="id" value="<?php echo $id?>"/>
                <input type="hidden" name="PanierId" value="<?php echo $panierId?>"/>
                <input type="hidden" name="ProduitId" value="<?php echo $lignecommandeObjet['ProduitId'] ?>"/>
                <input type="hidden" name="object" value="lignecommande" />
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Produit</th>
                            <td>
                                <select class="selectpicker" disabled="" data-title="Produit" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Produit = pg_fetch_row($resultProduit,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Produit['id']?>" <?php if($lignecommandeObjet['ProduitId']==$Produit['id']){ echo "selected='selected'";}?>><?php echo $Produit['libelle']?> (<?php echo $Produit['quantiteStock']?> dispo(s)</option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Quantité</th>
                            <td><input type="number" name="quantite" id="valeur" class="form-control" value="<?php echo $lignecommandeObjet['quantite']?>" /></td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-warning pull-right" value="Confirmer les modifications">
                </div>
            </form>
        </div>
    </div>
</div>