<?php 
require_once('../Modele/Panier/Panier.php');
require_once('../Modele/Panier/LigneCommande.php');
$id = $_GET['id'];
$panier = new Panier();
$table = $panier->getNomTable();
$panier->setNomTable("panier_libelle");
$results = $panier->rechercher(NULL, " AND ID='".$id."'");
$panierObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC);
$lignecommande = new LigneCommande();
$tableLigneCommande = $lignecommande->getNomTable();
$lignecommande->setNomTable("lignecommande_libelle");
$resultsLigneCommande = $lignecommande->rechercher(NULL, " AND \"PanierId\"='".$id."'");
?>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card">
            <div class="header" align="center">
                <h3 class="title"><a href="smarket.php?page=panier/panier-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Fiche Panier Numéro <?php echo $id?></h3>
            </div>
            <div class="content">
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Date Panier</th>
                            <td><?php echo $panierObjet['datePanier']?></td>
                        </tr>
                        <tr>
                            <th>Client</th>
                            <td><?php echo $panierObjet['clientId']?></td>
                        </tr>
                        <tr>
                            <th>Caisse</th>
                            <td><?php echo $panierObjet['caisseId']?></td>
                        </tr>
                        <tr>
                            <th>Etat</th>
                            <td><?php echo $panier->getEtatTexte($panierObjet['etat'])?></td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <a href="deleteGen.php?id=<?php echo $id?>&but=panier/panier-liste.php&nomTable=<?php echo $table?>" class="btn btn-danger pull-right">Supprimer</a>
                    <a href="smarket.php?id=<?php echo $id?>&page=panier/panier-update.php" class="btn btn-warning pull-right">Modifier</a>
                    <?php
                    if($panierObjet['etat']==1){
                    ?>
                        <a href="updateEtatGen.php?id=<?php echo $id?>&but=panier/panier-fiche.php&nomTable=<?php echo $table?>&etat=2" class="btn btn-primary pull-right">Panier sur la caisse</a>
                    <?php
                    }
                    else if($panierObjet['etat']==2){
                    ?>
                        <a href="updateEtatGen.php?id=<?php echo $id?>&but=panier/panier-fiche.php&nomTable=<?php echo $table?>&etat=1" class="btn btn-warning pull-right">Annuler le panier sur la caisse</a>
                        <a href="updateEtatGen.php?id=<?php echo $id?>&but=panier/panier-fiche.php&nomTable=<?php echo $table?>&etat=3" class="btn btn-primary pull-right">Payer le panier</a>
                    <?php
                    }
                    else if($panierObjet['etat']==3){
                    ?>
                        <a href="updateEtatGen.php?id=<?php echo $id?>&but=panier/panier-fiche.php&nomTable=<?php echo $table?>&etat=2" class="btn btn-warning pull-right">Annuler le paiement</a>
                        <a href="updateEtatGen.php?id=<?php echo $id?>&but=panier/panier-fiche.php&nomTable=<?php echo $table?>&etat=4" class="btn btn-primary pull-right">Livrer le panier</a>
                    <?php
                    }
                    else{
                    ?>
                        <a href="updateEtatGen.php?id=<?php echo $id?>&but=panier/panier-fiche.php&nomTable=<?php echo $table?>&etat=3" class="btn btn-warning pull-right">Annuler livraison</a>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card">
        <div class="content">
            <a href="smarket.php?page=panier/lignecommande-saisie.php&id=<?php echo $id ?>" class="btn btn-primary pull-right"><i class="ti-plus"></i> Ajouter</a>
            <div class="fresh-datatables">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>Identification</th>
                        <th>Produit</th>
                        <th>Quantité</th>
                        <th class="disabled-sorting">Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Identification</th>
                        <th>Produit</th>
                        <th>Quantité</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while($ligneCommandeObjet = pg_fetch_row($resultsLigneCommande,NULL, PGSQL_ASSOC)){ ?>
                    <tr>
                        <td><?php echo $ligneCommandeObjet['id']?></td>
                        <td><?php echo $ligneCommandeObjet['ProduitId']?></td>
                        <td><?php echo $ligneCommandeObjet['quantite']?></td>
                        <td>
                            <a href="smarket.php?page=panier/lignecommande-update.php&id=<?php echo $ligneCommandeObjet['id']?>" class="btn btn-simple btn-warning btn-icon"><i class="ti-pencil-alt"></i></a>
                            <a href="deleteGen.php?id=<?php echo $ligneCommandeObjet['id']?>&but=<?php echo $_GET['page']?>&nomTable=<?php echo $tableLigneCommande?>&returnId=<?php echo $id?>" class="btn btn-simple btn-danger btn-icon"><i class="ti-close"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
