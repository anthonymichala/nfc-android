<?php
require('../Modele/Users/Identification.php');
session_start();
$identification = new Identification();
$identification->setId($_SESSION['idLogin']);
$identification->updateEtat(NULL, 1);
session_destroy();
header('Location: ../index.php');
