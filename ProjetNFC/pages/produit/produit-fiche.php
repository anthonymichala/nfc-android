<?php 
require_once('../Modele/Produit/Produit.php');
$id = $_GET['id'];
$produit = new Produit();
$table = $produit->getNomTable();
$produit->setNomTable("produit_libelle");
$results = $produit->rechercher(NULL, " AND ID='".$id."'");
$produitObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC);
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=produit/produit-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Fiche Produit Numéro <?php echo $id?></h3>
        </div>
        <div class="content">
            <div class="row">
                <table class="table table-bordered">
                    <tr>
                        <th>Libellé</th>
                        <td><?php echo $produitObjet['libelle']?></td>
                    </tr>
                    <tr>
                        <th>Image</th>
                        <td><img src="../assets/img/product/<?php echo $produitObjet['image']?>"></td>
                    </tr>
                    <tr>
                        <th>Montant</th>
                        <td><?php echo $produitObjet['montant']?></td>
                    </tr>
                    <tr>
                        <th>Quantité en Stock</th>
                        <td><?php echo $produitObjet['quantiteStock']?></td>
                    </tr>
                    <tr>
                        <th>Rayon</th>
                        <td><?php echo $produitObjet['RayonId']?></td>
                    </tr>
                    <tr>
                        <th>Etat</th>
                        <td><?php echo $produit->getEtatTexte($produitObjet['etat'])?></td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <a href="deleteGen.php?id=<?php echo $id?>&but=produit/produit-liste.php&nomTable=<?php echo $table?>" class="btn btn-danger pull-right">Supprimer</a>
                <a href="smarket.php?page=produit/produit-update.php&id=<?php echo $produitObjet['id']?>" class="btn btn-warning pull-right">Modifier</a>
                <?php
                if($produitObjet['etat']==0){
                ?>
                    <a href="updateEtatGen.php?id=<?php echo $id?>&but=produit/produit-fiche.php&nomTable=<?php echo $table?>&etat=1" class="btn btn-primary pull-right">Rendre disponible</a>
                <?php
                }
                else{
                ?>
                    <a href="updateEtatGen.php?id=<?php echo $id?>&but=produit/produit-fiche.php&nomTable=<?php echo $table?>&etat=0" class="btn btn-danger pull-right">Rendre indisponible</a>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>