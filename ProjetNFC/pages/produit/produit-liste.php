<?php
require('../Modele/Produit/Produit.php');
$produit = new Produit();
$table = $produit->getNomTable();
$produit->setNomTable("produit_libelle");
$results = $produit->rechercher(NULL, "");
?>
<h1>Liste des produits</h1>
<div class="card">
    <div class="content">
        <a href="smarket.php?page=produit/produit-saisie.php" class="btn btn-primary pull-right"><i class="ti-plus"></i> Ajouter</a>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
                <tr>
                    <th>Identification</th>
                    <th>Image</th>
                    <th>Libelle</th>
                    <th>Montant</th>
                    <th>Quantité en stock</th>
                    <th>Rayon</th>
                    <th>Etat</th>
                    <th class="disabled-sorting">Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Identification</th>
                    <th>Libelle</th>
                    <th>Image</th>
                    <th>Montant</th>
                    <th>Quantité en stock</th>
                    <th>Rayon</th>
                    <th>Etat</th>
                    <th>Actions</th>
                </tr>   
            </tfoot>
            <tbody>
                <?php while($produitObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC)){ ?>
                <tr>
                    <td><a href="smarket.php?page=produit/produit-fiche.php&id=<?php echo $produitObjet['id']?>"><?php echo $produitObjet['id']?></a></td>
                    <td><img src="../assets/img/product/<?php echo $produitObjet['image']?>"></td>
                    <td><?php echo $produitObjet['libelle']?></td>
                    <td><?php echo $produitObjet['montant']?></td>
                    <td><?php echo $produitObjet['quantiteStock']?></td>
                    <td><?php echo $produitObjet['RayonId']?></td>
                    <td><?php echo $produit->getEtatTexte($produitObjet['etat'])?></td>
                    <td>
                        <a href="smarket.php?page=produit/produit-update.php&id=<?php echo $produitObjet['id']?>" class="btn btn-simple btn-warning btn-icon"><i class="ti-pencil-alt"></i></a>
                        <a href="deleteGen.php?id=<?php echo $produitObjet['id']?>&but=<?php echo $_GET['page']?>&nomTable=<?php echo $table?>" class="btn btn-simple btn-danger btn-icon"><i class="ti-close"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</div>