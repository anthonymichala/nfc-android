<?php

require_once('../Service/ProduitService.php');
$action = $_POST['action'];
$id = "";
$produitService = new ProduitService();
$fiche = "smarket.php?";
$libelle = $_POST['libelle'];
$montant = $_POST['montant']; 
$quantiteStock = $_POST['quantiteStock'];
$rayonId = $_POST['rayonId'];
if($action=="insert"){
    $id = $produitService->insertProduit(NULL, $libelle, $montant, $quantiteStock, $rayonId);
}
else if($action=="update"){
    $id = $_POST['id'];
    $produitService->updateProduit(NULL, $id, $libelle, $montant, $quantiteStock, $rayonId);
}
if(isset($_FILES['image']['tmp_name']) && $_FILES['image']['tmp_name']!=""){
    $nomFichierServeur = $_FILES['image']['tmp_name'];
    $type = $_FILES['image']['type'];
    $produitService->updateImageProduit($nomFichierServeur, $type, $id);
}
$fiche .= "page=produit/produit-fiche.php&id=".$id;
?>
<script>
   //document.location.replace("<?php echo $fiche ?>");
</script>
