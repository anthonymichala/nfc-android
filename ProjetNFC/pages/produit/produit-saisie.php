<?php 
require_once('../Modele/Rayon/Rayon.php');
$rayon = new Rayon();
$resulltRayon = $rayon->rechercher(NULL, "");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=produit/produit-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Ajout Produit </h3>
        </div>
        <div class="content">
            <form method="post" action="smarket.php?page=produit/apresProduit.php" enctype="multipart/form-data">
                <input type="hidden" name="page" value="produit/apresProduit.php"/>
                <input type="hidden" name="action" value="insert"/>
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Image</th>
                            <td>
                                <input type="file" name="image" id="file-1" class="inputfile btn btn-primary" data-multiple-caption="{count} files selected" multiple />
                                <label class="btn btn-primary" for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Changer l'image du produit</span></label>
                            </td>
                        </tr>
                        <tr>
                            <th>Libelle</th>
                            <td><input type="text" name="libelle" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Montant</th>
                            <td><input type="text" name="montant" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Quantité Stock</th>
                            <td><input type="text" name="quantiteStock" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Rayon</th>
                            <td>
                                <select name="rayonId" class="selectpicker" data-title="Rayon" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($Rayon = pg_fetch_row($resulltRayon,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $Rayon['id']?>" ><?php echo $Rayon['valeur']?></option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-warning pull-right" value="Confirmer les modifications">
                </div>
            </form>
        </div>
    </div>
</div>