<?php 
require_once('../Modele/Rayon/Rayon.php');
require_once('../Modele/Users/Identification.php');
$identification = new Identification();
$resultsIdentification = $identification->rechercher(NULL, " AND \"typeIdentification\"=4");
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=rayon/rayon-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Ajouter un Rayon</h3>
        </div>
        <div class="content">
            <form method="get" action="smarket.php">
                <input type="hidden" name="page" value="rayon/apresRayon.php"/>
                <input type="hidden" name="action" value="insert"/>
                <div class="row">
                    <table class="table table-bordered">
                        <tr>
                            <th>Valeur</th>
                            <td><input type="text" name="valeur" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><input type="text" name="description" id="valeur" class="form-control" /></td>
                        </tr>
                        <tr>
                            <th>Chef de Rayon</th>
                            <td>
                                <select name="ChefRayonId" class="selectpicker" data-title="Chef De Rayon" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                    <?php while($ChefRayon = pg_fetch_row($resultsIdentification,NULL, PGSQL_ASSOC)){
                                    ?>
                                        <option value="<?php echo $ChefRayon['id']?>" ><?php echo $ChefRayon['nom']." ".$ChefRayon['prenom']?></option>  
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-success pull-right" value="Ajouter" />
                </div>
            </form>
        </div>
    </div>
</div>