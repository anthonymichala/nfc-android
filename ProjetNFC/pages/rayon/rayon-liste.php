<?php
require('../Modele/Rayon/Rayon.php');
$rayon = new Rayon();
$table = $rayon->getNomTable();
$rayon->setNomTable("rayon_libelle");
$results = $rayon->rechercher(NULL, "");
?>
<h1>Liste des rayons</h1>
<div class="card">
    <div class="content">
        <a href="smarket.php?page=rayon/rayon-saisie.php" class="btn btn-primary pull-right"><i class="ti-plus"></i> Ajouter</a>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
                <tr>
                    <th>Identification</th>
                    <th>Nom</th>
                    <th>Description</th>
                    <th>Chef de Rayon</th>
                    <th class="disabled-sorting">Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Identification</th>
                    <th>Nom</th>
                    <th>Description</th>
                    <th>Chef de Rayon</th>
                    <th>Actions</th>
                </tr>   
            </tfoot>
            <tbody>
                <?php while($rayonObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC)){ ?>
                <tr>
                    <td><a href="smarket.php?page=rayon/rayon-fiche.php&id=<?php echo $rayonObjet['id']?>"><?php echo $rayonObjet['id']?></a></td>
                    <td><?php echo $rayonObjet['valeur']?></td>
                    <td><?php echo $rayonObjet['description']?></td>
                    <td><?php echo $rayonObjet['ChefRayonId']?></td>
                    <td>
                        <a href="smarket.php?id=<?php echo $rayonObjet['id']?>&page=rayon/rayon-update.php" class="btn btn-simple btn-warning btn-icon"><i class="ti-pencil-alt"></i></a>
                        <a href="deleteGen.php?id=<?php echo $rayonObjet['id']?>&but=<?php echo $_GET['page']?>&nomTable=<?php echo $table?>" class="btn btn-simple btn-danger btn-icon"><i class="ti-close"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</div>