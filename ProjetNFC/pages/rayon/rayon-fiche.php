<?php 
require_once('../Modele/Rayon/Rayon.php');
$id = $_GET['id'];
$rayon = new Rayon();
$table = $rayon->getNomTable();
$rayon->setNomTable("rayon_libelle");
$results = $rayon->rechercher(NULL, " AND ID='".$id."'");
$rayonObjet = pg_fetch_row($results,NULL, PGSQL_ASSOC);
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="card">
        <div class="header" align="center">
            <h3 class="title"><a href="smarket.php?page=rayon/rayon-liste.php" class="btn btn-primary"><i class="ti-back-left"></i></a> Fiche Rayon Numéro <?php echo $id?></h3>
        </div>
        <div class="content">
            <div class="row">
                <table class="table table-bordered">
                    <tr>
                        <th>Valeur</th>
                        <td><?php echo $rayonObjet['valeur']?></td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td><?php echo $rayonObjet['description']?></td>
                    </tr>
                    <tr>
                        <th>Chef de Rayon</th>
                        <td><?php echo $rayonObjet['ChefRayonId']?></td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <a href="deleteGen.php?id=<?php echo $id?>&but=rayon/rayon-liste.php&nomTable=<?php echo $table?>" class="btn btn-danger pull-right">Supprimer</a>
                <a href="smarket.php?id=<?php echo $id?>&page=rayon/rayon-update.php" class="btn btn-warning pull-right">Modifier</a>
            </div>
        </div>
    </div>
</div>