<?php
session_start();
if(!isset($_SESSION['login'])){
    ?>
<script>
    alert("Veuillez vous connecter pour accéder à ce contenu");
    document.location.href="../index.php";
</script>
<?php
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Smarket</title>

    <!-- Canonical SEO -->
    <link rel="canonical" href="https://www.creative-tim.com/product/paper-dashboard-pro"/>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <?php include('elements/css.php'); ?>
</head>

<body>
	<div class="wrapper">
            <?php include('elements/menu.php'); ?>
	    <div class="main-panel">
	        <?php include('elements/entete.php'); ?>

	        <div class="content">
	            <div class="container-fluid">
	                <?php
                        try{
                            include("".$_GET['page']."");
                        } catch (Exception $ex) {
                            ?>
                            <script>
                                alert('<?php echo $ex->getMessage() ?>');
                                history.back();
                            </script>
                        <?php
                        }
                        ?>
	            </div>
	        </div>

	        <?php include('elements/footer.php'); ?>
	    </div>
	</div>s
</body>
<?php include('elements/js.php'); ?>

</html>
