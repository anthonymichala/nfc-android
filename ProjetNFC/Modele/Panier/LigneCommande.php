<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LigneCommande
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../DataBean/BaseModele.php');
class LigneCommande extends BaseModele{
    private $quantite, $Panier_id, $Produit_id;
    function __construct() {
        parent::setNomTable("LigneCommande");
    }

    function getQuantite() {
        return $this->quantite;
    }

    function getPanier_id() {
        return $this->Panier_id;
    }

    function getProduit_id() {
        return $this->Produit_id;
    }

    function setQuantite($quantite) {
        if(!is_numeric($quantite)){
            throw new Exception("Quantité non numérique");
        }
        if($quantite<=0){
            throw new Exception("Quantité inférieur ou égale à 0");
        }
        $this->quantite = $quantite;
    }

    function setPanier_id($Panier_id) {
        if($Panier_id==NULL){
            throw new Exception("Le Panier ne peut être nul");
        }
        $this->Panier_id = $Panier_id;
    }

    function setProduit_id($Produit_id) {
        if($Produit_id==NULL){
            throw new Exception("Le Produit ne peut être nul");
        }
        $this->Produit_id = $Produit_id;
    }

}
