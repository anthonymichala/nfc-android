<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Panier
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../DataBean/BaseModeleEtat.php');
class Panier extends BaseModeleEtat{
    private $id, $datePanier, $Client_id, $Caisse_id;
    
    function __construct() {
        parent::setNomTable("Panier");
    }
    function getId() {
        return $this->id;
    }

    function getDatePanier() {
        return $this->datePanier;
    }

    function getClient_id() {
        return $this->Client_id;
    }

    function getCaisse_id() {
        return $this->Caisse_id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDatePanier($datePanier) {
        if($datePanier==NULL){
            throw new Exception("Date du Panier ne peu être nul");
        }
        $this->datePanier = $datePanier;
    }

    function setClient_id($Client_id) {
        if($Client_id==NULL){
            throw new Exception("Client ne peut être nul");
        }
        $this->Client_id = $Client_id;
    }

    function setCaisse_id($Caisse_id) {
        if($this->getEtat()>1 && $Caisse_id==NULL){
            throw new Exception("La champ caisse ne peut être vide si un client à déjà fini son chargement");
        }
        $this->Caisse_id = $Caisse_id;
    }


    function getEtatTexte($etat){
        switch ($etat){
            case 1:
                $etat="En cours de chargement";
                break;
            case 2:
                $etat="A la caisse";
                break;
            case 3:
                $etat="Payé";
                break;
            case 4:
                $etat="Livré";
                break;
        }
        return $etat;
    }
}
