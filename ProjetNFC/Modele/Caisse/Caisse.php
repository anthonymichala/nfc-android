<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Caisse
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../DataBean/BaseModeleEtat.php');
class Caisse extends BaseModeleEtat{
    private $numeroCaisse, $remarque, $Caissier_id;
    
    function __construct() {
        parent::setNomTable("Caisse");
    }

    function getNumeroCaisse() {
        return $this->numeroCaisse;
    }

    function getRemarque() {
        return $this->remarque;
    }

    function getCaissier_id() {
        return $this->Caissier_id;
    }

    function setNumeroCaisse($numeroCaisse) {
        if($numeroCaisse==NULL){
            throw new Exception("Le Numéro de Caisse ne peut être nul");
        }
        else{
            $this->numeroCaisse = $numeroCaisse;
        }
    }

    function setRemarque($remarque) {
        $this->remarque = $remarque;
    }

    function setCaissier_id($Caissier_id) {
        $this->Caissier_id = $Caissier_id;
    }
    function getEtatTexte($etat){
        switch ($etat){
            case 1:
                $etat="Libre";
                break;
            case 0:
                $etat="Caisse Hors Service";
                break;
            case 2:
                $etat="En cours d'utilisation";
                break;
        }
        return $etat;
    }

}
