<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rayon
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../DataBean/BaseModele.php');
class Rayon extends BaseModele{
    private $valeur, $description, $Chef_Rayon_id;
    function __construct() {
        parent::setNomTable("Rayon");
    }

    function getValeur() {
        return $this->valeur;
    }

    function getDescription() {
        return $this->description;
    }

    function getChef_Rayon_id() {
        return $this->Chef_Rayon_id;
    }

    function setValeur($valeur) {
        $this->valeur = $valeur;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setChef_Rayon_id($Chef_Rayon_id) {
        $this->Chef_Rayon_id = $Chef_Rayon_id;
    }


}
