<?php
require_once('../DataBean/BaseModeleEtat.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Identification
 *
 * @author anthonyandrianaivoravelona
 */
class Identification extends BaseModeleEtat{
    private $login, $mdp,$nom,$prenom,$image,$typeIdentification;
    
    function __construct() {
        parent::setNomTable("Identification");
    }

    function getLogin() {
        return $this->login;
    }

    function getMdp() {
        return $this->mdp;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getImage() {
        return $this->image;
    }

    function getTypeIdentification() {
        return $this->typeIdentification;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setMdp($mdp) {
        $this->mdp = $mdp;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setTypeIdentification($typeIdentification) {
        $this->typeIdentification = $typeIdentification;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }
    function getEtatTexte($etat){
        switch ($etat){
            case 1:
                $etat="Déconnecté";
                break;
            case 0:
                $etat="Compte supprimé";
                break;
            case 2:
                $etat="Connecté sur le site d'administration";
                break;
            case 3:
                $etat="Connecté sur l'application mobile";
                break;
        }
        return $etat;
    }

}
