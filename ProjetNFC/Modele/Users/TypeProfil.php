<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TypeProfil
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../DataBean/BaseModele.php');
class TypeProfil extends BaseModele{
    private $id, $val, $desce;
    function __construct() {
        parent::setNomTable("TypeProfil");
    }

    function getVal() {
        return $this->val;
    }

    function getDesce() {
        return $this->desce;
    }

    function setVal($val) {
        if($val==NULL){
            throw new Exception("La valeur ne peut être négative");
        }
        else{
            $this->val = $val;
        }
    }

    function setDesce($desce) {
        $this->desce = $desce;
    }


}
