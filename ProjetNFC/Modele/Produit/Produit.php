<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Produit
 *
 * @author anthonyandrianaivoravelona
 */
require_once ('../DataBean/BaseModeleEtat.php');
class Produit extends BaseModeleEtat{
    private $libelle, $image, $montant, $quantiteStock, $Rayon_id;
    
    function __construct() {
        parent::setNomTable("Produit");
    }
    

    function getLibelle() {
        return $this->libelle;
    }

    function getImage() {
        return $this->image;
    }

    function getMontant() {
        return $this->montant;
    }

    function getQuantiteStock() {
        return $this->quantiteStock;
    }

    function getRayon_id() {
        return $this->Rayon_id;
    }

    function getEtat() {
        return $this->etat;
    }

    function setLibelle($libelle) {
        if($libelle==NULL){
            throw new Exception("Le champ libelle ne peut être vide");
        }
        $this->libelle = $libelle;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setMontant($montant) {
        if(!is_numeric($montant)){
            throw new Exception("Montant non numéric");
        }
        if($montant<=0){
            throw new Exception("Le montant ne peut ni être égale à 0 ni négatif");
        }
        $this->montant = $montant;
    }

    function setQuantiteStock($quantiteStock) {
        if(!is_numeric($quantiteStock)){
            throw new Exception("Quantité en stock non numéric");
        }
        if($quantiteStock<0){
            throw new Exception("La quantité en stock du produit ne peut être négatif");
        }
        $this->quantiteStock = $quantiteStock;
    }

    function setRayon_id($Rayon_id) {
        $this->Rayon_id = $Rayon_id;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }

    function getEtatTexte($etat){
        switch ($etat){
            case 0:
                $etat="Non disponible";
                break;
            case 1:
                $etat="Disponible";
                break;
        }
        return $etat;
    }

}
