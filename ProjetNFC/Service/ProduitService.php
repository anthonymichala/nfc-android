<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProduitService
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../Modele/Produit/Produit.php');
require_once ('../Utilitaire/Utilitaire.php');
class ProduitService {
    public static function majStock($connection, $produit, $quantite){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $newproduit = new Produit();
            $result = $newproduit->rechercher($connection, " AND id=".$produit);
            $produitObjet = pg_fetch_row($result,NULL, PGSQL_ASSOC);
            $newproduit->setQuantiteStock($produitObjet['quantiteStock']-$quantite);
            $query = "UPDATE \"".$newproduit->getNomTable()."\" set \"quantiteStock\"=".$newproduit->getQuantiteStock()." WHERE id=".$produit."";
            pg_query($connection, $query);
        }
        catch (Exception $ex) {
            throw $ex;
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updateImageProduit($nomFichierSurServeur, $typeFichier, $id){
        try{
            $utilConnex = new UtilitaireConnexion();
            $connection = $utilConnex->getConn();
            $chemin = "../assets/img/product/";
            $produit = new Produit();
            $produit->setId($id);
            DBGen::updatePhoto($connection, $produit, $nomFichierSurServeur, $chemin, $id.".".Utilitaire::splitTypeFichier($typeFichier));
        }
        catch (Exceptions $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($connection != NULL){
                pg_close($connection);
            }
        }
        
    }
    public function updateProduit($connection, $id, $libelle, $montant, $quantiteStock, $rayon){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            
            $produit = new Produit();
            $produit->setLibelle($libelle);
            $produit->setMontant($montant);
            $produit->setQuantiteStock($quantiteStock);
            $produit->setRayon_id($rayon);
            $query = "UPDATE \"".$produit->getNomTable()."\" set libelle='".$produit->getLibelle()."', montant=".$produit->getMontant().", \"quantiteStock\"=".$produit->getQuantiteStock().", \"RayonId\"='".$produit->getRayon_id()."' WHERE id=".$id."";
            pg_query($connection, $query);
            if($_SESSION['idLogin']==$id){
                $_SESSION['login'] = $nom." ".$prenom;
            }
            return $id;
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function insertProduit($connection, $libelle, $montant, $quantiteStock, $rayon){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $produit = new Produit();
            $produit->setId($produit->getValSequence($connection));
            $produit->setLibelle($libelle);
            $produit->setMontant($montant); 
            $produit->setQuantiteStock($quantiteStock);
            $produit->setRayon_id($rayon);
            $query = "INSERT INTO \"".$produit->getNomTable()."\" VALUES(".$produit->getId().", '".$produit->getLibelle()."','default.png', ".$produit->getMontant().",".$produit->getQuantiteStock().",1,".$produit->getRayon_id().")";
            echo $query;
            pg_query($connection, $query);
            return $produit->getId();
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
}
