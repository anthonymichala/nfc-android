<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RayonService
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../Modele/Rayon/Rayon.php');
class RayonService {
    public function insertRayon($connection, $valeur, $description, $ChefRayonId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $rayon = new Rayon();
            $rayon->setId($rayon->getValSequence($connection));
            $rayon->setValeur($valeur);
            $rayon->setDescription($description);
            $rayon->setChef_Rayon_id($ChefRayonId);
            $query = "INSERT INTO \"".$rayon->getNomTable()."\" VALUES(".$rayon->getId().", '".$rayon->getValeur()."', '".$rayon->getDescription()."', ".$rayon->getChef_Rayon_id().")";
            pg_query($connection, $query);
            return $rayon->getId();
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updateRayon($connection, $id, $valeur, $description, $ChefRayonId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $rayon = new Rayon();
            $rayon->setValeur($valeur);
            $rayon->setDescription($description);
            $rayon->setChef_Rayon_id($ChefRayonId);
            $query = "UPDATE \"".$rayon->getNomTable()."\" set valeur='".$rayon->getValeur()."', description='".$rayon->getDescription()."', \"ChefRayonId\"=".$rayon->getChef_Rayon_id()." WHERE id=".$id."";
            pg_query($connection, $query);
            return $id;
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
}
