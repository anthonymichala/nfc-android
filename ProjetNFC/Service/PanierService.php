<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PanierService
 *
 * @author anthonyandrianaivoravelona
 */
require_once ('../Utilitaire/UtilitaireConnexion.php');
require_once('../Modele/Panier/Panier.php');
require_once('../Modele/Panier/LigneCommande.php');
require_once('../Service/ProduitService.php');
class PanierService {
    public function insertPanier($connection, $datePanier, $clientId, $caisseId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $panier = new Panier();
            $panier->setId($panier->getValSequence($connection));
            $panier->setDatePanier($datePanier);
            $panier->setClient_id($clientId);
            $panier->setCaisse_id($caisseId);
            $query = "INSERT INTO \"".$panier->getNomTable()."\" VALUES(".$panier->getId().", '".$panier->getDatePanier()."', ".$panier->getClient_id().", ".$panier->getCaisse_id().",1)";
            pg_query($connection, $query);
            return $panier->getId();
        } 
        catch (Exception $ex) {
            throw $ex;
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updatePanier($connection, $id, $datePanier, $clientId, $caisseId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $panier = new Panier();
            $panier->setDatePanier($datePanier);
            $panier->setClient_id($clientId);
            $panier->setCaisse_id($caisseId);
            $query = "UPDATE \"".$panier->getNomTable()."\" set \"datePanier\"='".$panier->getDatePanier()."', \"clientId\"='".$panier->getClient_id()."', \"caisseId\"=".$panier->getCaisse_id()." WHERE id=".$id."";
            pg_query($connection, $query);
            return $id;
        } 
        catch (Exception $ex) {
            throw $ex;
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function checkEtatPanier($connection, $idPanier){
        $panier = new Panier();
        $resultPanier = $panier->rechercher($connection, " AND id=".$idPanier);
        $panierPrincipal = pg_fetch_row($resultPanier,NULL, PGSQL_ASSOC);
        if($panierPrincipal['etat']>=3){
            return FALSE;
        }
        return TRUE;
    }
    public function insertLigneCommande($connection, $produit, $quantite, $PanierId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            if(!$this->checkEtatPanier($connection, $PanierId)){
                throw new Exception("Impossible de modifier le panier car le client a déjà payer le montant de la facture");
            }
            $ligneCommande = new LigneCommande();
            $result = $ligneCommande->rechercher($connection, " AND \"ProduitId\"=".$produit." AND \"PanierId\"=".$PanierId);
            $num = pg_num_rows($result);
            if($num==0){
                $ligneCommande->setId($ligneCommande->getValSequence($connection));
                $ligneCommande->setProduit_id($produit);
                $ligneCommande->setQuantite($quantite);
                $ligneCommande->setPanier_id($PanierId);
                $query = "INSERT INTO \"".$ligneCommande->getNomTable()."\" VALUES(".$ligneCommande->getId().", ".$ligneCommande->getQuantite().", ".$ligneCommande->getPanier_id().", ".$ligneCommande->getProduit_id().")";
                pg_query($connection, $query);
                ProduitService::majStock($connection, $produit, $quantite);
                return $ligneCommande->getPanier_id();
            }
            else{
                $ligneCommandePrincipal = pg_fetch_row($result,NULL, PGSQL_ASSOC);
                $this->updateLigneCommande($connection, $ligneCommandePrincipal['id'], $ligneCommandePrincipal['ProduitId'],$ligneCommandePrincipal['quantite']+$quantite,$ligneCommandePrincipal['PanierId']);
                return $ligneCommandePrincipal['PanierId'];
            }
        } 
        catch (Exception $ex) {
            throw $ex;
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updateLigneCommande($connection, $id, $produit, $quantite, $PanierId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            if(!$this->checkEtatPanier($connection, $PanierId)){
                throw new Exception("Impossible de modifier le panier car le client a déjà payer le montant de la facture");
            }
            $ligneCommandeNew = new LigneCommande();
            $result = $ligneCommandeNew->rechercher($connection, " AND id=".$id);
            $ligneCommandePrincipal = pg_fetch_row($result,NULL, PGSQL_ASSOC);
            $quantiteInitial = $quantite-$ligneCommandePrincipal['quantite'];
            $ligneCommande = new LigneCommande();
            $ligneCommande->setProduit_id($produit);
            $ligneCommande->setQuantite($quantite);
            $ligneCommande->setPanier_id($PanierId);
            $query = "UPDATE \"".$ligneCommande->getNomTable()."\" set quantite='".$ligneCommande->getQuantite()."', \"ProduitId\"=".$ligneCommande->getProduit_id()." WHERE id=".$id."";
            pg_query($connection, $query);
            ProduitService::majStock($connection,$produit, $quantiteInitial);
            return $ligneCommande->getPanier_id();
        } 
        catch (Exception $ex) {
            throw $ex;
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
}
