<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CaisseService
 *
 * @author anthonyandrianaivoravelona
 */
require_once('../Modele/Caisse/Caisse.php');
class CaisseService {
    public function insertCaisse($connection, $numeroCaisse, $remarque, $CaissierId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $caisse = new Caisse();
            $caisse->setId($caisse->getValSequence($connection));
            $caisse->setNumeroCaisse($numeroCaisse);
            $caisse->setRemarque($remarque);
            $caisse->setCaissier_id($CaissierId);
            $query = "INSERT INTO \"".$caisse->getNomTable()."\" VALUES(".$caisse->getId().", '".$caisse->getNumeroCaisse()."', '".$caisse->getRemarque()."', ".$caisse->getCaissier_id().",1)";
            echo $query;
            pg_query($connection, $query);
            return $caisse->getId();
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updateCaisse($connection, $id, $numeroCaisse, $remarque, $CaissierId){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $caisse = new Caisse();
            $caisse->setNumeroCaisse($numeroCaisse);
            $caisse->setRemarque($remarque);
            $caisse->setCaissier_id($CaissierId);
            $query = "UPDATE \"".$caisse->getNomTable()."\" set \"numeroCaisse\"='".$caisse->getNumeroCaisse()."', remarque='".$caisse->getRemarque()."', \"caissierId\"=".$caisse->getCaissier_id()." WHERE id=".$id."";
            pg_query($connection, $query);
            return $id;
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
}
