<?php
require_once('../Modele/Users/Identification.php');
require_once('../Modele/Users/TypeProfil.php');
require_once('../DataBean/DBGen.php');
require_once('../Utilitaire/Utilitaire.php');
class IdentificationService{
    public function testLogin($connection, $pseudo, $mdp){
        $identification = new Identification();
        $identifications = $identification->rechercher($connection, " AND login='".$pseudo."' AND mdp='".$mdp."'");
        $personnel = pg_fetch_array($identifications);
        $num = pg_num_rows($identifications);
        if($num==0){
            throw new Exception("Login ou mot de passe erroné");
        }
        else{
            return $personnel;
        }
    }
    public function insertTypeProfil($connection, $valeur, $description){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $typeProfil = new TypeProfil();
            $typeProfil->setId($typeProfil->getValSequence($connection));
            $typeProfil->setVal($valeur);
            $typeProfil->setDesce($description);
            $query = "INSERT INTO \"".$typeProfil->getNomTable()."\" VALUES(".$typeProfil->getId().", '".$typeProfil->getVal()."', '".$typeProfil->getDesce()."')";
            pg_query($connection, $query);
            return $typeProfil->getId();
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updateTypeProfil($connection, $id, $valeur, $description){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $typeProfil = new TypeProfil();
            $typeProfil->setVal($valeur);
            $typeProfil->setDesce($description);
            $query = "UPDATE \"".$typeProfil->getNomTable()."\" set val='".$typeProfil->getVal()."', desce='".$typeProfil->getDesce()."' WHERE id=".$id."";
            pg_query($connection, $query);
            return $id;
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function insertUser($connection, $login, $mdp, $nom, $prenom, $typeIdentification){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            $identification = new Identification();
            $identification->setId($identification->getValSequence($connection));
            $identification->setLogin($login);
            $identification->setMdp($mdp);
            $identification->setNom($nom);
            $identification->setPrenom($prenom);
            $identification->setTypeIdentification($typeIdentification);
            $query = "INSERT INTO \"".$identification->getNomTable()."\" VALUES(".$identification->getTypeIdentification().", '".$identification->getPrenom()."', '".$identification->getNom()."','".$identification->getMdp()."','".$identification->getLogin()."','default.png',".$identification->getId().",1)";
            pg_query($connection, $query);
            return $identification->getId();
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    public function updateUser($connection, $id, $login, $mdp, $nom, $prenom, $typeIdentification){
        $test = FALSE;
        try{
            if($connection==null){
                $utilConnex = new UtilitaireConnexion();
                $connection = $utilConnex->getConn();
                $test = TRUE;
            }
            
            $identification = new Identification();
            $identification->setLogin($login);
            $identification->setMdp($mdp);
            $identification->setNom($nom);
            $identification->setPrenom($prenom);
            $identification->setTypeIdentification($typeIdentification);
            $query = "UPDATE \"".$identification->getNomTable()."\" set \"typeIdentification\"=".$identification->getTypeIdentification().", prenom='".$identification->getPrenom()."', nom='".$identification->getNom()."', mdp='".$identification->getMdp()."', login='".$identification->getLogin()."' WHERE id=".$id."";
            pg_query($connection, $query);
            if($_SESSION['idLogin']==$id){
                $_SESSION['login'] = $nom." ".$prenom;
            }
            return $id;
        } 
        catch (Exception $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($test && $connection != NULL){
                pg_close($connection);
            }
        }
    }
    
    public function updatePdpClient($nomFichierSurServeur, $typeFichier, $id){
        try{
            $utilConnex = new UtilitaireConnexion();
            $connection = $utilConnex->getConn();
            $chemin = "../assets/img/users/";
            $identification = new Identification();
            $identification->setId($id);
            DBGen::updatePhoto($connection, $identification, $nomFichierSurServeur, $chemin, $id.".".Utilitaire::splitTypeFichier($typeFichier));
            
        }
        catch (Exceptions $ex) {
            throw $ex->getMessage();
        }
        finally {
            if($connection != NULL){
                pg_close($connection);
            }
        }
        
    }
}