<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UtilitaireConnexion
 *
 * @author anthonyandrianaivoravelona
 */
class UtilitaireConnexion {
    public function getConn(){
        $host = "localhost";
        $port = "5432";
        $dbname = "nfc";
        $user = "postgres";
        $password = "angel03";
        $pg_options = "--client_encoding=UTF8";
        $connection_string = "host={$host} port={$port} dbname={$dbname} user={$user} password={$password} options='{$pg_options}'";
        $dbconn = pg_connect($connection_string);
        if (!$dbconn) {
            throw new Exception("Erreur de connexion");
        } else {
            return $dbconn;
        }
    }
}
