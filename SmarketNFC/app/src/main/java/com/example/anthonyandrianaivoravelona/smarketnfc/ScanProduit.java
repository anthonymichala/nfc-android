package com.example.anthonyandrianaivoravelona.smarketnfc;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ScanProduit extends AppCompatActivity {
    Button buttonMoins, buttonPlus;
    EditText editText;
    TextView textView15, prixProduit, libelle;
    double prix=0.00;
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    Activity activity;
    String url ;
    Button addProduit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_produit);
        buttonMoins = (Button) findViewById(R.id.button4);
        buttonPlus = (Button) findViewById(R.id.button6);
        editText = (EditText) findViewById(R.id.editText8);
        textView15 = (TextView) findViewById(R.id.textView15);
        prixProduit = (TextView) findViewById(R.id.textViewprixProduit);
        libelle = (TextView) findViewById(R.id.TextViewLibelleProduit);
        addProduit = (Button) findViewById(R.id.button1);
        activity = this;
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent =   PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        url  = Constante.getUrlProduit();
        if (!nfcAdapter.isEnabled())
        {
            Alert.showLocationDialog(activity,"Attention","NFC non activé");
        }

        buttonMoins.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                editText.setText(String.valueOf(Integer.valueOf(editText.getText().toString())-1));
                textView15.setText(String.valueOf(prix*Integer.valueOf(editText.getText().toString())));
            }
        });
        buttonPlus.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                editText.setText(String.valueOf(Integer.valueOf(editText.getText().toString())+1));
                textView15.setText(String.valueOf(prix*Integer.valueOf(editText.getText().toString())));
            }
        });
        addProduit.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Alert.showLocationDialog(activity,"Succes","Le produit a ete ajoute dans le panier");
            }
        });
    }


    public void onNewIntent(Intent intent)
    {

        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action) || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action))
        {
            processNfcIntent(intent) ;
        }
    }


    public void processNfcIntent (Intent intent)
    {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        byte[] id =tag.getId();
        String[] technologies = tag.getTechList();
        int content = tag.describeContents();
        Ndef ndef = Ndef.get(tag);
        boolean isWritable = ndef.isWritable();
        boolean canMakeReadOnly = ndef.canMakeReadOnly();
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        NdefMessage[] msgs;
        String message = null;
        if (rawMsgs != null)
        {
            msgs = new NdefMessage[rawMsgs.length];
            System.out.println("msgs = " + msgs);
            System.out.println(" length = "+rawMsgs.length);
            for (int i = 0; i < rawMsgs.length; i++)
            {
                msgs[i] = (NdefMessage) rawMsgs[i];
                NdefRecord record = msgs[i].getRecords()[i];
                byte[] idRec = record.getId();
                short tnf = record.getTnf();
                byte[] type = record.getType();
                message = new String (record.getPayload());
                System.out.println("message = '" + message+"'");

            }
            message = message.trim();
            message = String.valueOf(message.charAt(2));
            url += "?id="+message;
            if(message.equals("1")){
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try   {
                            System.out.println("url = " + url);
                            String reponse = "1;libelle;1";
                            System.out.println("reponse = " + reponse);
                            String[] retour = Utilitaire.splitProduit(reponse);
                            libelle.setText(retour[1]);
                            prixProduit.setText(String.valueOf(retour[2]));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this,pendingIntent, null,null);
    }


    @Override
    public void onPause()
    {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

}
