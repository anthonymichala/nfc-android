package com.example.anthonyandrianaivoravelona.smarketnfc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by anthonyandrianaivoravelona on 27/04/2017.
 */

public class Alert {
    public static void showLocationDialog(Activity act, String titre, String contenue) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle(titre);
        builder.setMessage(contenue);

        String positiveText = "ok";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                    }
                });
        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
}
