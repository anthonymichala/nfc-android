package com.example.anthonyandrianaivoravelona.smarketnfc;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



/**
 * Created by macbookpro on 28/03/2017.
 */

public class RetrieveMessagesString extends AsyncTask<String, Void, String> {
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    public RetrieveMessagesString(){
    }


    @Override
    protected String doInBackground(String... urls) {
        HttpClient client = new DefaultHttpClient();
        String json = "";
        try {

            String line = "";
            HttpGet request = new HttpGet(urls[0]);
            HttpResponse response = client.execute(request);
            response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            while ((line = rd.readLine()) != null) {
                System.out.println("Line = "+line);
                json += line;
            }

        } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return json;
    }

    @Override
    protected void onProgressUpdate(Void... progress) {
//               Log.i("ANDRANA 4","progress");
    }

    @Override
    public void onPostExecute(String test){

    }
}
