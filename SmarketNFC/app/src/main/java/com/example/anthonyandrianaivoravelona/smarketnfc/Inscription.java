package com.example.anthonyandrianaivoravelona.smarketnfc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Inscription extends AppCompatActivity {

    Button buttonConnexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        buttonConnexion = (Button) findViewById(R.id.button2);

        buttonConnexion.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(Inscription.this, ScanProduit.class));
            }
        });
    }

}
